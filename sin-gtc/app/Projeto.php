<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    protected $fillable = [
      'idRequerente', 'tipoProjeto', 'idEmpreendimento', 'pastaDocumentos', 'situacao', 'dataFinal',
    ];
}
