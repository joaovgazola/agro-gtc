<?php

namespace App\Http\Controllers\Auth;

use App\Entidade;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Local;
use App\Projeto;
use App\Representante;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Dompdf\Dompdf;

class EmpresaRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {

        return view('auth.registrar');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'cnpj' => ['required', 'string'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'cnpj' => $data['cnpj'],
        ]);
    }
    protected function guard()
    {
        return Auth::guard('web');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public  function user(Request $request){
        return view('auth.userEmpresa', ['requerenteid'=>$request->requerente, 'entidadeid'=> $request->entidade,'endRequerente'=>$request->endRequerente, 'endEntidade'=>$request->endEntidade, 'projetoid'=>$request->projeto]);
    }

    public function pdfGerador($nome, $cpf, $idLocalRepresentante, $atividade, $idLocalEntidade, $tipoRequerimento){
        switch ($tipoRequerimento){
            case 1:
                $requerimento = "realize a an&aacute;lise de projeto e registro da ind&uacute;stria de ";
                break;
            case 2:
                $requerimento = "realize a instala&ccedil;&atilde;o do SIM - Servi&ccedil;o de Inspe&ccedil;&atilde;o Municipal na ind&uacute;stria de ";
                break;
            case 3:
                $requerimento = "me conceda a licen&ccedil;a para a amplia&ccedil;&atilde;o da ind&uacute;stria de ";
                break;
            case 4:
                $requerimento = "realize a an&aacute;lise de projeto e registro da ind&uacute;stria de ";
                break;
        }
        $local1 = Local::findOrFail($idLocalEntidade);
        $local2 = Local::findOrFail($idLocalRepresentante);
        return '<p style="text-align: right;">'.$local1->cidade.', '.date('d/m').' de '.date('Y').'.</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: left;"><strong>Ilustr&iacute;ssimo Senhor Prefeito Municipal</strong></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>Requerimento</strong></span></p>
<p style="text-align: left;">Eu, '.$nome.', portador do CPF '.$cpf.'. residente na '.$local2->logradouro.', numero '.$local2->numero.' em '.$local2->cidade.', '.$local2->uf.', venho atrav&eacute;s desta, mui respeitosamente solicitar que <em>Vossa Senhoria&nbsp;</em>'.$requerimento.$atividade.', localizada na '.$local1->logradouro.' numero '.$local1->numero.', neste municipio, onde a mesma ser&aacute; inspecionada pelo Servi&ccedil;o de Inspe&ccedil;&atilde;o Municipal-SIM</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Nestes termos,</p>
<p style="text-align: left;">Pede Deferimento</p>';
    }

    public function baixarpdf(Request $request){
        $pdf = new Dompdf();
        $pdf->loadHtml($this->pdfGerador($request->nomeRepresentante, $request->cpfRepresentante, $request->enderecoRequerente, $request->atividade, $request->enderecoEntidade, $request->tipoRequerimento));
        $pdf->render();
        $pdf->stream();
        return redirect()->back();
    }

    public function requerimento(){
        $projeto = new Projeto();
        $projeto->save();
        Session::put('projeto', $projeto->id);
        return view('auth.empresa.requerimento');
    }
    public function entidade(Request $request){

        return view('auth.empresa.entidade');
    }
    public function representante(Request $request){
        $entidade = new Entidade();
        $entidade->nomeEntidade = $request->nomeEntidade;
        $entidade->cnpj = $request->cnpj;
        $entidade->valor = $request->dinheiro;
        $entidade->atividade = $request->atividade;
        $entidade->descAtividade = $request->descricao;
        $enderecoEntidade = new Local();
        $enderecoEntidade->cep = $request->cepEntidade;
        $enderecoEntidade->logradouro = $request->logradouroEntidade;
        $enderecoEntidade->numero = $request->numeroEntidade;
        $enderecoEntidade->bairro = $request->bairroEntidade;
        $enderecoEntidade->cidade = $request->cidadeEntidade;
        $enderecoEntidade->uf = $request->ufEntidade;
        $enderecoEntidade->referencia = $request->referenciaEntidade;
        $enderecoEntidade->save();
        $entidade->idLocal = $enderecoEntidade->id;
        $entidade->save();
        $projeto = Projeto::findOrFail(Session::get('projeto'));
        $projeto->idEntidade = $entidade->id;
        $projeto->save();
        return view('auth.empresa.representante');
    }
    public function tipoprojeto(Request $request){
        $representante = new Representante();
        $representante->nomeRepresentante = $request->nomeRepresentante;
        $representante->cpfRepresentante = $request->cpfRepresentante;
        $representante->cargoRepresentante = $request->cargoRepresentante;
        $representante->telefoneRepresentante = $request->telefoneRepresentante;
        $representante->faxRepresentante = $request->faxRepresentante;
        $representante->celularRepresentante = $request->celularRepresentante;
        $representante->emailRepresentante = $request->emailRepresentante;
        if($request->yesno == 2){
            $representante->autorizado = 2;
            $representante->nomenao = $request->nomenao;
            $representante->cpfnao = $request->cpfnao;
            $representante->telefonenao = $request->telefonenao;
            $representante->emailnao = $request->emailnao;
        }
        else{
            $representante->autorizado = 1;
        }
        $enderecoRepresentante = new Local();
        $enderecoRepresentante->cep = $request->cepRepresentante;
        $enderecoRepresentante->logradouro = $request->logradouroRepresentante;
        $enderecoRepresentante->numero = $request->numeroRepresentante;
        $enderecoRepresentante->bairro = $request->bairroRepresentante;
        $enderecoRepresentante->cidade = $request->cidadeRepresentante;
        $enderecoRepresentante->uf = $request->ufRepresentante;
        $enderecoRepresentante->referencia = $request->referenciaRepresentante;
        $enderecoRepresentante->save();
        $representante->idLocal = $enderecoRepresentante->id;
        $representante->save();

        $projeto = Projeto::findOrFail(Session::get('projeto'));
        $entidade = Entidade::findOrFail($projeto->idEntidade);
        $entidade->idRepresentante = $representante->id;
        $entidade->save();
        return view('auth.empresa.tipoprojeto');
    }
    public function requerente(Request $request){
        $projeto = Projeto::findOrFail(Session::get('projeto'));
        $projeto->tipoRequerimento = $request->tipoRequerimento;
        $projeto->save();
        return view('auth.empresa.requerente');
    }
    public function licenca(Request $request){
        $requerente = new Representante();
        $requerente->nomeRepresentante = $request->nomeRequerente;
        $requerente->cpfRepresentante = $request->cpfRequerente;
        $requerente->cargoRepresentante = $request->cargoRequerente;
        $requerente->telefoneRepresentante = $request->telefoneRequerente;
        $requerente->faxRepresentante = $request->faxRequerente;
        $requerente->celularRepresentante = $request->celularRequerente;
        $requerente->emailRepresentante = $request->emailRequerente;
        $requerente->autorizado = 3;
        $enderecoRequerente = new Local();
        $enderecoRequerente->cep = $request->cepRequerente;
        $enderecoRequerente->logradouro = $request->logradouroRequerente;
        $enderecoRequerente->numero = $request->numeroRequerente;
        $enderecoRequerente->bairro = $request->bairroRequerente;
        $enderecoRequerente->cidade = $request->cidadeRequerente;
        $enderecoRequerente->uf = $request->ufRequerente;
        $enderecoRequerente->referencia = $request->referenciaRequerente;
        $enderecoRequerente->save();
        $requerente->idLocal = $enderecoRequerente->id;
        $requerente->save();

        $projeto = Projeto::findOrFail(Session::get('projeto'));
        $projeto->idRequerente = $requerente->id;
        $projeto->save();
        return view('auth.empresa.licenca');
    }
    public function usuario(Request $request){
        $projeto = Projeto::findOrFail(Session::get('projeto'));
        $entidade = Entidade::findOrFail($projeto->idEntidade);
        if($request->yessnao == 2){
            $entidade->tipoLicenca = $request->tipoLicenca;
            $entidade->numerolicenca = $request->numerolicenca;
            $entidade->anoLicenca = $request->anoLicenca;
            $entidade->validadeLicenca = $request->validadeLicenca;
        }
        $entidade->save();
        $requerente = Representante::findOrFail($projeto->idRequerente);
        return view('auth.empresa.userEmpresa', ['name'=>$entidade->nomeEntidade, 'cnpj'=>$entidade->cnpj, 'email'=>$requerente->emailRepresentante]);
    }
}
