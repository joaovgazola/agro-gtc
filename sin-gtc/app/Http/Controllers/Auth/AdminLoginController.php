<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');;
    }

    public function showLoginForm(){
        return view('auth.adminLogin');
    }
    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|min:8',
        ]);
        if(Auth::guard('admin')->attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)){
            return redirect()->intended(route('admin'));
        }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
