<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Termo;
use Illuminate\Support\Facades\Session;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('prefeitura.admin');
    }
    public function editTermo(){
        $termo = Termo::firstOrCreate(['id' => '1']);
        return view('prefeitura.editTermo', ['termo'=>$termo]);
    }

    public function saveTermo(Request $request){
        $termo = Termo::FirstOrNew(['id' => '1']);
        $termo->delta = $request->hiddenArea;
        $termo->save();
        Session::flash('sucesso', 'Novo termo salvo com sucesso');
        return redirect()->to(route('editTermo'));
    }

    public function projeto()
    {
        return view('prefeitura.projeto');
    }

    public function parecer()
    {
        return view('prefeitura.parecer');
    }
}
