<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidade extends Model
{
    protected $fillable =[
        'nomeEntidade', 'cnpj', 'atividade', 'descAtividade', 'valor', 'tipoLicenca', 'numeroLicenca', 'anoLicenca', 'validadeLicenca', 'idLocal', 'idRepresentante',
    ];
}
