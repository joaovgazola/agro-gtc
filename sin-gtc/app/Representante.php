<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representante extends Model
{
    protected $fillable =[
        'nomeRepresentante', 'cpfRepresentante', 'cargoRepresentante', 'telefoneRepresentante', 'faxRepresentante', 'celularRepresentante', 'emailRepresentante', 'idLocal', 'autorizado', 'nomenao', 'cpfnao', 'telefonenao', 'emailnao',
    ];
}
