@extends('layouts.app')

@section('content')
<div class="row col m-t-40 justify-content-center">
    <div class="col-md-4 col-md-offset-4">
        <div class="card">
            

            <div class="card-body">
                <form method="POST" action="{{ route('adminLoginSubmit') }}"
                class="col-md-12 p-t-10">
                @csrf

                <h2 class="text-center m-b-20"><b>Login Prefeitura</b></h2>
                <hr/>

                <div class="form-group">
                    <label for="email" class="control-label"><strong>
                        {{ __('Usuário') }}
                    </strong></label>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-success" id="basic-addon1">
                                <i class="fa fa-user text-white"></i>
                            </span>
                        </div>

                        <input id="email" type="text" class="form-control @error('user') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror  
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="control-label"><strong>
                        {{ __('Senha') }}
                    </strong></label>

                    <div class="input-group">
                        <div class="input-group-prepend ">
                            <span class="input-group-text bg-success" id="basic-addon1">
                                <i class="fas fa-lock text-white"></i>
                            </span>
                        </div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror  
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Lembre-me') }}
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="btn btn-success">
                            {{ __('Entrar') }}
                        </button>
                        @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Esqueceu sua Senha?') }}
                        </a>
                        @endif
                    </div>   
                </div>
                <hr/>
                <div class="form-group">
                    <div class="input-group"> 
                        <p>
                            Novo usuário?   
                            <a href="{{route('register')}}"
                            onclick="window.location.href = '/register'"> Registrar</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
