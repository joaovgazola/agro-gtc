@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row m-t-40 justify-content-center">
        <div class="col-md-6 col-md-offset-6">
            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-b-20"><b>Redefinir Senha</b></h3>
                    <hr/>


                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}"
                     class="col-md-12 p-t-10">
                        @csrf

                        <div class="form-group">
                            <strong>
                               <label for="email" class="control-label">
                               {{ __('E-Mail') }}
                               </label> 
                            </strong>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                               <i class="fas fa-envelope"></i>
                                </span>
                                </div>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <br>
                            <div class="text-center">
                               <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Enviar') }} <i class="fas fa-paper-plane"></i>
                            </button> 
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
