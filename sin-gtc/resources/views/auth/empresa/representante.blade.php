@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <br>
      <br>
      <div class="card">

        <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
          <div class="panel-heading">
            <ul class="nav nav-tabs border-bottom-0">

              <li class="disabled nav-link border border-bottom-0">
               Requerimento 
             </li>
             <li class="disabled nav-link border border-bottom-0 ml-1">
              Entidade
            </li>
            <li class="active nav-link bg-success text-white border border-bottom-0 ml-1">
              Representante
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Tipo do projeto
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Requerente
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Licença
            </li>

            <li class="disabled nav-link border border-bottom-0 ml-1">
              Usuário
            </li>



          </ul>
        </div>

      </div>

      <div class="card-body">
        <div class="container col-12">

         <form action="{{route('tipoprojeto')}}" method="post">
          @csrf
          <h4>
           Representante Legal da Entidade
         </h4>
         <hr/>

         <div class="row">
           <div class="col-lg-3">
             <strong>
               <label for="nomeRepresentante">Nome
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="text" name="nomeRepresentante" id="nomeRepresentante" class="form-control" required>
           </div>
           <div class="col-lg-3">
             <strong>
               <label for="cpfRepresentante">CPF
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="text" name="cpfRepresentante" id="cpfRepresentante" class="form-control" 
             onkeydown="javascript: fMasc( this, mCPF );"required
             maxlength="14"
             placeholder="000.000.000-00">
           </div>

           <div class="col-lg-3">
             <strong>
               <label for="cargoRepresentante">Cargo
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="text" name="cargoRepresentante" id="cargoRepresentante" class="form-control" required>
           </div>

           <div class="col-lg-3">
             <strong>
               <label for="emailRepresentante">Email
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="email" name="emailRepresentante" id="emailRepresentante" class="form-control" required>
           </div>
         </div>
         <br>
         <div class="row">
           <div class="col-lg-4">
             <strong>
               <label for="telefoneRepresentante">Telefone
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="text" name="telefoneRepresentante" id="telefoneRepresentante" class="form-control" placeholder="(00)0000-0000"
             maxlength="14"
             onkeydown="javascript: fMasc( this, mTel );"
             required>

           </div>

           <div class="col-lg-4">
             <strong>
               <label for="celularRepresentante">Celular
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="text" name="celularRepresentante" id="celularRepresentante" class="form-control"
             placeholder="(00)00000-0000" maxlength="14"
             onkeydown="javascript: fMasc( this, mTel );"
             required>

           </div>

           <div class="col-lg-4">
             <strong>
               <label for="faxRepresentante">Outro Contato</label>
             </strong>

             <input type="text" name="faxRepresentante" id="faxRepresentante"
             class="form-control"
             placeholder="(00)0000-0000"
             maxlength="14"
             onkeydown="javascript: fMasc( this, mTel );"><br>
           </div>
         </div>
         <br>
         <h4>
           Endereço do representante
         </h4>
         <hr/>
         <div class="row">

           <div class="col-lg-3">
             <strong>
               <label for="cepRepresentante">CEP
                 <strong style="color: red;">*</strong>
               </label>
             </strong>
             <input type="text" name="cepRepresentante" id="cepRepresentante"class="form-control"
             placeholder="00000-000"
             maxlength="10"
             onkeydown="javascript: fMasc( this, mCEP );" required>

             <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>
           </div>

           <div class="col-lg-5">
             <strong>
               <label for="logradouroRepresentante">Logradouro</label>
             </strong>
             <input type="text" name="logradouroRepresentante" id="logradouroRepresentante" class="form-control"  readonly required><br>

           </div>

           <div class="col-lg-3">
             <strong>
               <label for="cidadeRepresentante">Cidade</label>
             </strong>
             <input type="text" name="cidadeRepresentante" id="cidadeRepresentante" class="form-control" readonly required><br>
           </div>

           <div class="col-lg-1">
             <strong>
               <label for="ufRepresentante">Estado</label>
             </strong>
             <input type="text" name="ufRepresentante" id="ufRepresentante" class="form-control"  readonly required><br>
           </div>

         </div>

         <div class="row">
           <div class="col-lg-2">
             <strong>
               <label for="numeroRepresentante">Número
                 <strong style="color: red;">*</strong>
               </label>
             </strong>

             <input type="number" name="numeroRepresentante" id="numeroRepresentante" class="form-control" min="1" required><br>
           </div>

           <div class="col-lg-4">
             <strong>
               <label for="bairroRepresentante">Bairro</label>
             </strong>

             <input type="text" name="bairroRepresentante" id="bairroRepresentante" class="form-control"  readonly required><br>
           </div>
           <div class="col-lg-6">
             <strong>
               <label for="referenciaRepresentante">Referência</label>
             </strong>

             <input type="text" name="referenciaRepresentante" id="referenciaRepresentante" class="form-control">

           </div>

           <div class="col-lg-12">
             <p>O representante está autorizado a receber o documento solicitado? (Caso selecione o "não", preencha todos os campos que aparecerão, do contrario o representante legal será autorizado a receber o documento).
             </p>
           </div>

           <div class="container">
            <div class="row">
              <div class="col-lg-12">


                <input id="yes" name="collapseGroup" type="radio" data-toggle="collapse" data-target="#collapseOne" checked value="1" /> Sim

                <input id="no" name="collapseGroup" type="radio" data-toggle="collapse" data-target="#collapseOne" value="2" /> Não


                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">

                    <div id="collapseOne" class="panel-collapse collapse">
                      <br>
                      <div class="panel-body">
                        <h4>
                         Quem vai receber?
                       </h4>
                       <hr/>
                       <div class="row">

                         <div class="col-lg-4">
                           <strong>
                             <label for="nomenao">Nome
                               <strong style="color: red;">*</strong>
                             </label>
                           </strong>

                           <input id="nomenao" type="text" name="nomenao" class="form-control" >

                         </div>
                         <div class="col-lg-3">
                           <strong>
                             <label for="emailnao">Email
                               <strong style="color: red;">*</strong>
                             </label>
                           </strong>

                           <input id="emailnao" type="email" name="emailnao" class="form-control"  >

                         </div>

                         <div class="col-lg-3">
                           <strong>
                             <label for="cpfnao">CPF
                               <strong style="color: red;">*</strong>
                             </label>
                           </strong>

                           <input id="cpfnao" type="text" name="cpfnao" class="form-control"
                           
                           onkeydown="javascript: fMasc( this, mCPF );"
                           maxlength="14" placeholder="000.000.000-00"
                           >

                         </div>

                         <div class="col-lg-2">
                           <strong>
                             <label for="tel">Telefone
                               <strong style="color: red;">*</strong>
                             </label>
                           </strong>
                           <input type="text" name="telefonenao" class="form-control" id="telefonenao"
                           
                           placeholder="(00)0000-0000" maxlength="13"
                           onkeydown="javascript: fMasc( this, mTel );"
                           >

                         </div>

                         
                         <hr/>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>

             </div>
           </div>
         </div>

       </div>
       <hr/>
       <div class="progress">
         <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
         style="width: 48%;">
         Passo 3
       </div>
     </div>
     <br>
     <a class="btn btn-success btnPrevious" style="color: #fff;" href="entidade" >Voltar</a>
     <input type="submit" class="btn btn-success float-right" value="Próximo">
   </form>

 </div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

  var yesRadio = $('#yes'),
  noRadio = $('#no');

  yesRadio.click(function () {

    document.getElementById("nomenao").value = "";
    document.getElementById("emailnao").value = "";
    document.getElementById("cpfnao").value = "";
    document.getElementById("telefonenao").value = "";

    
    document.getElementById("nomenao").required = false;
    document.getElementById("emailnao").required = false;
    document.getElementById("cpfnao").required = false;
    document.getElementById("telefonenao").required = false;

    if($('#collapseOne').hasClass('in')) {

      return false;
    }
  });

  noRadio.click(function () {

    document.getElementById("nomenao").required = true;
    document.getElementById("emailnao").required  = true; 
    document.getElementById("cpfnao").required  = true;
    document.getElementById("telefonenao").required  = true;

    if(!$('#collapseOne').hasClass('in')) {

      return true;
    }
  });
</script>
@endsection