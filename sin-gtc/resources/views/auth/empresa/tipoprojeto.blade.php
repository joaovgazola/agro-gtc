@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <br>
      <br>
      <div class="card">

       <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
        <div class="panel-heading">
          <ul class="nav nav-tabs">

            <li class="disabled nav-link border border-bottom-0">
             Requerimento 
           </li>
           <li class="disabled nav-link border border-bottom-0 ml-1">
            Entidade
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Representante
          </li>
          <li class="active nav-link bg-success text-white border border-bottom-0 ml-1">
            Tipo do projeto
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Requerente
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Licença
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Usuário
          </li>



        </ul>
      </div>

    </div>

    <div class="card-body">
      <div class="container col-12">

       <form action="{{route('requerente')}}" method="post">
        @csrf
        <h4>
          Tipo do projeto a ser requisitado
        </h4>
        <hr/>
        
        <div class="table-responsive">
          <table class="table table-hover table-bordered"> 
            <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
              <tr>
                
               
                <th width="20%">
                 
                 Selecionar
               </th>
               <th></th>
               
             </tr>
           </thead>
           
           
           <tbody style="text-align: center;">
             
            <tr>
              <td>
                <input type="radio" name="tipoRequerimento" value="1" id="tipoRequerimento" required>
              </td>
              <td width="95%">Análise de Projeto e Registro de Estabelecimento</td>
            </tr>

            <tr>
              <td>
                <input type="radio"  name="tipoRequerimento" value="2" id="tipoRequerimento">
              </td>
              <td width="95%">Instalação do SIM - Serviço de Inspeção Municipal no Estabelecimento</td>
            </tr>

            <tr>
              <td>
                <input type="radio"  name="tipoRequerimento" value="3" id="tipoRequerimento">
              </td>
              <td width="95%">Licença para solicitação de ampliação de Estabelecimento</td>
            </tr>

            <tr>
              <td>
                <input type="radio"  name="tipoRequerimento" value="4" id="tipoRequerimento">
              </td>
              <td width="95%">Análise de Projeto e Registro de Estabelecimento</td>
            </tr>
            
          </tbody>
        </table>
      </div>
      <br>
      <hr/>
      <div class="progress">
        <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
        style="width: 64%;">
        Passo 4
      </div>
    </div>

    <br>
    
    
    <a class="btn btn-success btnPrevious " href="representante" style="color: #fff;">Voltar</a>
    <input type="submit" class="btn btn-success float-right" value="Próximo">
  </form>

</div>
</div>
</div>
</div>
</div>
</div>
@endsection