@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <br>
      <br>
      <div class="card">

       <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
        <div class="panel-heading">
          <ul class="nav nav-tabs border-bottom-0">

            <li class="active nav-link bg-success text-white border border-bottom-0">
             Requerimento 
           </li>
           <li class="disabled nav-link  border border-bottom-0 ml-1">
            Entidade
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Representante
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Tipo do projeto
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Requerente
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Licença
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Usuário
          </li>



        </ul>
      </div>

    </div>

    <div class="card-body">
      <div class="container col-12">

       <form action="{{route('entidade')}}" method="post">
        @csrf
        <h4>
         Requerimento Padrão
       </h4>

       <hr/>

       <div id="quill"></div>
       <br>
       <div class="progress">
        <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
        style="width: 16%;">
        Passo 1 
      </div>
    </div>
    <br>

    <input type="checkbox" id="checkTermos" name="checkTermos" aria-label="Checkbox for following text input" required class="float-left mt-1"> <p> Li e confirmo as informações dando ciência e conhecimento.

    </p>

    <a href="{{route('login')}}" class="btn btn-success"> Voltar</a>
    <input type="submit" class="btn btn-success float-right" value="Próximo">

  </form>

</div>
</div>
</div>
</div>
</div>
</div>

<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script>
  var quill = new Quill('#quill', {
    theme: 'snow',
    readOnly: true,
    modules: {
      toolbar: false
    }

  });
  <?php
  $termo = \App\Termo::first();
  if(!empty($termo->delta)){
    echo 'quill.setContents('.$termo->delta.')';
  }
  ?>
</script>
@endsection