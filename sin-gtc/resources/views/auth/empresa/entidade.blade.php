@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <br>
      <br>
      <div class="card">

       <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
        <div class="panel-heading" >
          <ul class="nav nav-tabs border-bottom-0">

            <li class="disabled nav-link border border-bottom-0">
             Requerimento 
           </li>
           
           <li class="active nav-link bg-success text-white border border-bottom-0 ml-1">
            Entidade
          </li>

          <li class="disabled nav-link border border-bottom-0 ml-1">
            Representante
          </li>

          <li class="disabled nav-link border border-bottom-0 ml-1">
            Tipo do projeto
          </li>

          <li class="disabled nav-link border border-bottom-0 ml-1">
            Requerente
          </li>

          <li class="disabled nav-link border border-bottom-0 ml-1">
            Licença
          </li>

          <li class="disabled nav-link border border-bottom-0 ml-1">
            Usuário
          </li>



        </ul>
      </div>

    </div>


    <div class="card-body">
      <div class="container col-12">

       <form action="{{route('representante')}}", method="post">
        @csrf
        <h4>
         Entidade
       </h4>
       <hr/>

       <div class="row">

         <div class="col-lg-4">
           <strong>
             <label for="nomeEntidade">Nome
               <strong style="color: red;">*</strong>
             </label>
           </strong>

           <input id="nomeEntidade" type="text" name="nomeEntidade" class="form-control" required>

         </div>
         <div class="col-lg-4">
           <strong>
             <label for="cnpj">CNPJ
               <strong style="color: red;">*</strong>
             </label>
           </strong>

           <input id="cnpj" type="text" name="cnpj" class="form-control" required placeholder="00.000.000/0000-00">

         </div>
         <div class="col-lg-4">
           <strong>
             <label for="valor">Valor de Investimento
               <strong style="color: red;">*</strong>
             </label>
           </strong>

           

           <div class="input-group">
            <div class="input-group-append">
              <span class="input-group-text">R$</span>
            </div>
            <input type="text" id="dinheiro" name="dinheiro" class="dinheiro form-control" required>
            
          </div>


        </div>
      </div>
      <br>
      <div class="row">
       <div class="col-lg-4 float-left">
         <strong>
           <label for="atividade">Atividade
             <strong style="color: red;">*</strong>
           </label>
         </strong>

         <input id="atividade" type="text" name="atividade" class="form-control" required>

       </div>
       <div class="col-lg-8">
         <strong>
           <label for="descricao">Descrição da Atividade
           </label>
         </strong>
         <textarea class="form-control" rows="1" cols="50" name="descricao" id="descricao" required>
         </textarea>
       </div>

     </div>

     <br>
     <h4>
       Endereço da entidade
     </h4>
     <hr/>
     <div class="row">

       <div class="col-lg-3">
         <strong>
           <label for="cepEntidade">CEP
             <strong style="color: red;">*</strong>
           </label>
         </strong>
         <input type="text" name="cepEntidade" id="cepEntidade"class="form-control"
         placeholder="00000-000"
         maxlength="10"
         onkeydown="javascript: fMasc( this, mCEP );" required>

         <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>
       </div>

       <div class="col-lg-5">
         <strong>
           <label for="logradouroEntidade">Logradouro</label>
         </strong>
         <input type="text" name="logradouroEntidade" id="logradouroEntidade" class="form-control"  readonly required><br>

       </div>

       <div class="col-lg-3">
         <strong>
           <label for="cidadeEntidade">Cidade</label>
         </strong>
         <input type="text" name="cidadeEntidade" id="cidadeEntidade" class="form-control" readonly required><br>
       </div>

       <div class="col-lg-1">
         <strong>
           <label for="ufEntidade">Estado</label>
         </strong>
         <input type="text" name="ufEntidade" id="ufEntidade" class="form-control"  readonly><br>
       </div>

     </div>

     <div class="row">
       <div class="col-lg-2">
         <strong>
           <label for="numeroEntidade">Número
             <strong style="color: red;">*</strong>
           </label>
         </strong>

         <input type="number" name="numeroEntidade" id="numeroEntidade" class="form-control" min="1" required><br>
       </div>

       <div class="col-lg-4">
         <strong>
           <label for="bairroEntidade">Bairro</label>
         </strong>

         <input type="text" name="bairroEntidade" id="bairroEntidade" class="form-control"  readonly required><br>
       </div>
       <div class="col-lg-6">
         <strong>
           <label for="referenciaEntidade">Referência</label>
         </strong>

         <input type="text" name="referenciaEntidade" id="referenciaEntidade" class="form-control">

       </div>
     </div>

     <hr/>
     <div class="progress">
       <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
       style="width: 32%;">
       Passo 2
     </div>
   </div>
   <br>
   <a class="btn btn-success btnPrevious" style="color: #fff;" href="requerimento">Voltar</a>
   <input type="submit" class="btn btn-success float-right" value="Próximo">
 </form>

</div>
</div>
</div>
</div>
</div>
</div>

<script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
<script src="https://unpkg.com/imask"></script>

<script type="text/javascript">

  var maskCnpj = IMask(document.getElementById('cnpj'), {
    mask:[
    {
      mask: '00.000.000/0000-00',
      maxlength: 14
    }
    ]
  });

</script>

<script type="text/javascript">

  $('.dinheiro').mask('#.##0,00', {reverse: true});

</script>

@endsection