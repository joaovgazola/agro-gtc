@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <br>
            <br>
            <div class="card">

                <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs border-bottom-0">

                            <li class="disabled nav-link border border-bottom-0">
                             Requerimento 
                         </li>
                         <li class="disabled nav-link border border-bottom-0 ml-1">
                            Entidade
                        </li>
                        <li class="disabled nav-link border border-bottom-0 ml-1">
                            Representante
                        </li>
                        <li class="disabled nav-link border border-bottom-0 ml-1">
                            Tipo do projeto
                        </li>
                        <li class="disabled nav-link border border-bottom-0 ml-1">
                            Requerente
                        </li>
                        <li class="disabled nav-link border border-bottom-0 ml-1">
                            Licença
                        </li>

                        <li class="active nav-link bg-success text-white border border-bottom-0 ml-1">
                            Usuário
                        </li>



                    </ul>
                </div>

            </div>

            <div class="card-body">
                <div class="container col-12">

                 <form method="POST" action="{{ route('registerEmpresa') }}">
                    @csrf
                    <h4>
                        Definir senha
                    </h4>
                    <hr/>

                    <div class="row">

                        <div class="col-lg-4">

                            <strong>
                             <label for="name">Nome
                                 <strong style="color: red;"></strong>
                             </label> 
                         </strong>

                         <input id="name" value="{{$name}}" readonly type="text" class="form-control @error('name') is-invalid @enderror" name="name" readonly required autocomplete="name" autofocus>

                         @error('name')
                         <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="col-lg-4">

                        <strong>
                           <label for="cnpj">CNPJ
                               <strong style="color: red;"></strong>
                           </label> 
                       </strong>

                       <input id="cnpj" type="text" value="{{$cnpj}}" readonly class="form-control @error('cnpj') is-invalid @enderror" name="cnpj"readonly required autocomplete="cnpj">

                       @error('cnpj')
                       <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>

                <div class="col-lg-4">

                    <strong>
                        <label for="email">E-mail
                         <strong style="color: red;"></strong>
                     </label> 
                 </strong>

                 <input id="email" type="text" value="{{$email}}" readonly class="form-control @error('email') is-invalid @enderror" name="email" readonly required autocomplete="email">

                 @error('email')
                 <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <br>
        <div class="row">

            <div class="col-lg-4">

                <strong>
                    <label for="password">Senha
                     <strong style="color: red;">*</strong>
                 </label> 
             </strong>

             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" required>

             @error('password')
             <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>


        <div class="col-lg-4">

            <strong>
                <label for="password-confirm">Confirmar Senha
                 <strong style="color: red;">*</strong>
             </label> 
         </strong>

         <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
     </div>
 </div>
 <br>

      <div class="progress">
        <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
        style="width: 100%;">
        Passo 7
      </div>
    </div>

</div>
 <hr/>
 <div class="row">
    <div class="col-lg-12">
        <a class="btn btn-success btnPrevious " style="color: #fff;" href="licenca">Voltar</a>
        <button type="submit" class="btn btn-success float-right">
            {{ __('Registrar') }}
        </button>
    </div>
</div>
</form>


</div>
</div>
</div>
</div>
</div>
@endsection
