@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <br>
      <br>
      <div class="card">

        <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
          <div class="panel-heading">
            <ul class="nav nav-tabs border-bottom-0">

              <li class="disabled nav-link border border-bottom-0">
               Requerimento 
             </li>
             <li class="disabled nav-link border border-bottom-0 ml-1">
              Entidade
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Representante
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Tipo do projeto
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Requerente
            </li>
            <li class="active nav-link bg-success text-white border border-bottom-0 ml-1">
              Licença
            </li>
            <li class="disabled nav-link border border-bottom-0 ml-1">
              Usuário
            </li>



          </ul>
        </div>

      </div>

      <div class="card-body">
        <div class="container col-12">

         <form action="{{route('usuario')}}" method="post">
          @csrf
          <h4>
            Esta entidade já possui uma Licença ou Autorização?
          </h4>
          <hr/>


          <input id="no" name="collapseGroup" type="radio" data-toggle="collapse" data-target="#collapseOne" checked value="2"/> Não

          <input id="yes" name="collapseGroup" type="radio" data-toggle="collapse" data-target="#collapseOne" value="1" />  Sim, especificar


          <div class="panel-group" id="accordion">
            <div class="panel panel-default">

              <div id="collapseOne" class="panel-collapse collapse">

                <div class="panel-body">
                  <br>
                  <div class="row">

                    <div class="col-lg-4">
                      <strong>
                        <label for="tipoLicenca">Tipo
                         <strong style="color: red;">*</strong>
                       </label>
                     </strong>
                     <input type="text" name="tipoLicenca" id="tipoLicenca" class="form-control">

                   </div>

                   <div class="col-lg-2">

                    <strong>
                      <label for="numerolicenca">N° 
                        <strong style="color: red;">*</strong>
                      </label>
                    </strong>

                    <input type="number" name="numerolicenca" id="numerolicenca" class="form-control" min="1">
                  </div>

                  <div class="col-lg-3">
                    <strong>
                      <label for="anoLicenca">Ano
                        <strong style="color: red;">*</strong>
                      </label>
                    </strong>
                    <input type="text" name="anoLicenca" id="anoLicenca" class="form-control" maxlength="4">

                  </div>

                  <div class="col-lg-3">
                    <strong>
                      <label for="ano">Validade 
                       <strong style="color: red;">*</strong>
                     </label>
                   </strong>
                   <input type="date" name="validadeLicenca" id="validadeLicenca" class="form-control">

                 </div>

               </div>
             </div>
           </div>
         </div>
       </div>

       <hr/>
       <div class="progress">
        <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
        style="width: 100%;">
        Passo 6
      </div>
    </div>
    <br>                   
    <a class="btn btn-success btnPrevious" style="color: #fff;" href="requerente" >Voltar</a>
    <input type="submit" name="salvar" class="btn btn-success float-right" value="Próximo"><br>
  </form>

</div>
</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">

  var yesRadio = $('#yes'),
  noRadio = $('#no');

  yesRadio.click(function () {

    document.getElementById("tipoLicenca").required = true;
    document.getElementById("numerolicenca").required  = true; 
    document.getElementById("anoLicenca").required  = true;
    document.getElementById("validadeLicenca").required  = true;

    if($('#collapseOne').hasClass('in')) {

      return false;
    }
  });

  noRadio.click(function () {

    document.getElementById("tipoLicenca").value = "";
    document.getElementById("numerolicenca").value = "";
    document.getElementById("anoLicenca").value = "";
    document.getElementById("validadeLicenca").value = "";

    
    document.getElementById("tipoLicenca").required = false;
    document.getElementById("numerolicenca").required = false;
    document.getElementById("anoLicenca").required = false;
    document.getElementById("validadeLicenca").required = false;

    if(!$('#collapseOne').hasClass('in')) {

      return true;
    }
  });
</script>
@endsection