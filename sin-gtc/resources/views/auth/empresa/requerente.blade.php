@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <br>
      <br>
      <div class="card">

       <div class="panel with-nav-tabs panel-primary ml-0" style="margin-top: -40px;">
        <div class="panel-heading">
          <ul class="nav nav-tabs border-bottom-0">

            <li class="disabled nav-link border border-bottom-0">
             Requerimento 
           </li>
           <li class="disabled nav-link border border-bottom-0 ml-1">
            Entidade
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Representante
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Tipo do projeto
          </li>
          <li class="active nav-link bg-success text-white border border-bottom-0 ml-1">
            Requerente
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Licença
          </li>
          <li class="disabled nav-link border border-bottom-0 ml-1">
            Usuário
          </li>



        </ul>
      </div>

    </div>

    <div class="card-body">
      <div class="container col-12">

       <form method="post" action="{{route('licenca')}}">
        @csrf
        <h4>
         Requerente do projeto
       </h4>

       <hr/>

       <div class="row">
         <div class="col-lg-4">
           <strong>
             <label for="nomeRequerente">Nome
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="text" name="nomeRequerente" id="nomeRequerente" class="form-control" required>
         </div>
         <div class="col-lg-2">
           <strong>
             <label for="cpfRequerente">CPF
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="text" name="cpfRequerente" id="cpfRequerente" class="form-control" 
           onkeydown="javascript: fMasc( this, mCPF );"
           maxlength="14" placeholder="000.000.000-00"
           required>
         </div>

         <div class="col-lg-3">
           <strong>
             <label for="cargoRequerente">Cargo
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="text" name="cargoRequerente" id="cargoRequerente" class="form-control" required>
         </div>

         <div class="col-lg-3">
           <strong>
             <label for="emailRequerente">Email
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="email" name="emailRequerente" id="emailRequerente" class="form-control" required>
         </div>
       </div>
       <br>
       <div class="row">
         <div class="col-lg-4">
           <strong>
             <label for="telefoneRequerente">Telefone
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="text" name="telefoneRequerente" id="telefoneRequerente" class="form-control" placeholder="(00)0000-0000"
           maxlength="14"
           onkeydown="javascript: fMasc( this, mTel );"
           required>

         </div>

         <div class="col-lg-4">
           <strong>
             <label for="celularRequerente">Celular
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="text" name="celularRequerente" id="celularRequerente" class="form-control"
           placeholder="(00)00000-0000" maxlength="14"
           onkeydown="javascript: fMasc( this, mTel );"
           required>

         </div>

         <div class="col-lg-4">
           <strong>
             <label for="faxRequerente">Outro Contato</label>
           </strong>

           <input type="text" name="faxRequerente" id="faxRequerente"
           class="form-control"
           placeholder="(00)0000-0000"
           maxlength="14"
           onkeydown="javascript: fMasc( this, mTel );"><br>
         </div>
       </div>
       <br>
       <h4>
         Endereço do requerente
       </h4>
       <hr/>
       <div class="row">

         <div class="col-lg-3">
           <strong>
             <label for="cepRequerente">CEP
               <strong style="color: red;">*</strong>
             </label>
           </strong>
           <input type="text" name="cepRequerente" id="cepRequerente"class="form-control"
           placeholder="00000-000"
           maxlength="10"
           onkeydown="javascript: fMasc( this, mCEP );" required>

           <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>
         </div>

         <div class="col-lg-5">
           <strong>
             <label for="logradouroRequerente">Logradouro</label>
           </strong>
           <input type="text" name="logradouroRequerente" id="logradouroRequerente" class="form-control"  readonly required><br>

         </div>

         <div class="col-lg-3">
           <strong>
             <label for="cidadeRequerente">Cidade</label>
           </strong>
           <input type="text" name="cidadeRequerente" id="cidadeRequerente" class="form-control" readonly required><br>
         </div>

         <div class="col-lg-1">
           <strong>
             <label for="ufRequerente">Estado</label>
           </strong>
           <input type="text" name="ufRequerente" id="ufRequerente" class="form-control"  readonly required><br>
         </div>

       </div>

       <div class="row">
         <div class="col-lg-2">
           <strong>
             <label for="numeroRequerente">Número
               <strong style="color: red;">*</strong>
             </label>
           </strong>

           <input type="number" name="numeroRequerente" id="numeroRequerente" class="form-control" min="1" required><br>
         </div>

         <div class="col-lg-4">
           <strong>
             <label for="bairroRequerente">Bairro</label>
           </strong>

           <input type="text" name="bairroRequerente" id="bairroRequerente" class="form-control"  readonly required><br>
         </div>
         <div class="col-lg-6">
           <strong>
             <label for="referenciaRequerente">Referência</label>
           </strong>

           <input type="text" name="referenciaRequerente" id="referenciaRequerente" class="form-control">

         </div>
       </div>

       <hr/>

       <div class="progress">
         <div class="progress-bar progress-bar-success bg-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
         style="width: 80%;">
         Passo 5
       </div>
     </div>

     <br>

     <a class="btn btn-success btnPrevious " style="color: #fff;" href="tipoprojeto">Voltar</a>
     <input type="submit" class="btn btn-success float-right" value="Próximo">
   </form>

 </div>
</div>
</div>
</div>
</div>
</div>
@endsection