@extends('layouts.app')

@section('content')
<div class="row col m-t-40 justify-content-center">
    <div class="col-md-4 col-md-offset-4">
        <div class="card">

            <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <h2 class="text-center m-b-20"><b>Registrar Novo Usuário</b></h2>
                    <hr/>

                    <div class="form-group">
                        <label for="name" class="control-label"><strong>
                            {{ __('Nome') }}
                        </strong></label>

                        <div class="input-group">


                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="control-label"><strong>
                            {{ __('Usuário') }}
                        </strong></label>

                        <div class="input-group">
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="control-label"><strong>
                            {{ __('Senha') }}
                        </strong></label>

                        <div class="input-group">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="control-label"><strong>
                            {{ __('Confirmar Senha') }}
                        </strong></label>

                        <div class="input-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <button type="submit" class="btn btn-success col-12">
                                {{ __('Registrar') }}
                            </button>
                        </div>
                    </div>
                    <hr/>

                    <div class="form-group">
                        <div class="input-group"> 
                            <p>
                                Já possui conta?   
                                <a href="{{route('adminLogin')}}"
                                onclick="window.location.href = 'adminLogin'"> Entrar </a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection
