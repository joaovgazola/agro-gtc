<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

    <!-- icones -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .link > a:hover{
            color: #38c172;
        }


        a:hover{
            color: #38c172;
        }

        ul:hover{
            color: #38c172;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>


    <script type="text/javascript">
        function fMasc(objeto,mascara) {
            obj=objeto
            masc=mascara
            setTimeout("fMascEx()",1)
        }
        function fMascEx() {
            obj.value=masc(obj.value)
        }
        
        function mCPF(cpf){
            cpf=cpf.replace(/\D/g,"")
            cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
            cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
            cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
            return cpf
        }

        function mCEP(cep){
            cep=cep.replace(/\D/g,"")
            cep=cep.replace(/^(\d{2})(\d)/,"$1.$2")
            cep=cep.replace(/\.(\d{3})(\d)/,".$1-$2")
            return cep
        }

        function mRG(rg){
            rg=rg.replace(/\D/g, "")
            rg=rg.replace(/(\d{1})(\d)/, "$1.$2")
            rg=rg.replace(/(\d{3})(\d)/, "$1.$2")
            rg=rg.replace(/(\d{3})(\d{1,2})$/, "$1.$2")
            return rg

        }

        function mTel(contato) {
            contato=contato.replace(/\D/g,"")
            contato=contato.replace(/^(\d)/,"($1")
            contato=contato.replace(/(.{3})(\d)/,"$1)$2")
            if(contato.length == 9) {
                contato=contato.replace(/(.{1})$/,"-$1")
            } else if (contato.length == 10) {
                contato=contato.replace(/(.{2})$/,"-$1")
            } else if (contato.length == 11) {
                contato=contato.replace(/(.{3})$/,"-$1")
            } else if (contato.length == 12) {
                contato=contato.replace(/(.{4})$/,"-$1")
            } else if (contato.length > 12) {
                contato=contato.replace(/(.{4})$/,"-$1")
            }
            return contato;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>

    <!-- Adicionando Javascript -->
    <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cepEntidade").val(null);
                $("#logradouroEntidade").val(null);
                $("#bairroEntidade").val(null);
                $("#cidadeEntidade").val(null);
                $("#ufEntidade").val(null);
            }

            //Quando o campo cep perde o foco.
            $("#cepEntidade").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "" enquanto consulta webservice.
                        $("#logradouroEntidade").val("Encontrando cep...");
                        $("#bairroEntidade").val("");
                        $("#cidadeEntidade").val("");
                        $("#ufEntidade").val("");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#logradouroEntidade").val(dados.logradouro);
                                $("#bairroEntidade").val(dados.bairro);
                                $("#cidadeEntidade").val(dados.localidade);
                                $("#ufEntidade").val(dados.uf);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cepRepresentante").val(null);
                $("#logradouroRepresentante").val(null);
                $("#bairroRepresentante").val(null);
                $("#cidadeRepresentante").val(null);
                $("#ufRepresentante").val(null);
            }

            //Quando o campo cep perde o foco.
            $("#cepRepresentante").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "" enquanto consulta webservice.
                        $("#logradouroRepresentante").val("Encontrando cep...");
                        $("#bairroRepresentante").val("");
                        $("#cidadeRepresentante").val("");
                        $("#ufRepresentante").val("");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#logradouroRepresentante").val(dados.logradouro);
                                $("#bairroRepresentante").val(dados.bairro);
                                $("#cidadeRepresentante").val(dados.localidade);
                                $("#ufRepresentante").val(dados.uf);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cepRequerente").val(null);
                $("#logradouroRequerente").val(null);
                $("#bairroRequerente").val(null);
                $("#cidadeRequerente").val(null);
                $("#ufRequerente").val(null);
            }

            //Quando o campo cep perde o foco.
            $("#cepRequerente").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "" enquanto consulta webservice.
                        $("#logradouroRequerente").val("Encontrando cep...");
                        $("#bairroRequerente").val("");
                        $("#cidadeRequerente").val("");
                        $("#ufRequerente").val("");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#logradouroRequerente").val(dados.logradouro);
                                $("#bairroRequerente").val(dados.bairro);
                                $("#cidadeRequerente").val(dados.localidade);
                                $("#ufRequerente").val(dados.uf);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>
    
</head>
<body>
    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

            <div class="nav-header" 
            style=" background-color:#28a745; 
            margin-right: 0;
            margin-left: -16px; 
            padding: 10px 10px;
            margin-top: -10px; 
            margin-bottom: -8px;">

            <span class="navbar-brand text-white">
                Sin-AGRO
            </span> 
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @guest
            <ul class="navbar-nav mr-auto">
                <li>
                    <a href="{{route('welcome')}}" class="nav-link"> Home </a>
                </li>
            </ul>

            @else

            @if(isset(Auth::user()->cnpj))
            <ul class="navbar-nav mr-auto">
                <li>
                    <a href="{{route('admin')}}" class="nav-link"> teste </a>
                </li>

            </ul>
            @else 

            <ul class="navbar-nav mr-auto">
                <li>
                    <a href="{{route('admin')}}" class="nav-link"> Requerimento </a>
                </li>

                <li>
                    <a href="{{route('projeto')}}" class="nav-link"> Projetos </a>
                </li>

            </ul>
            @endif
            @endguest
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest

                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                       <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                       <i class="fas fa-sign-out-alt"></i> {{ __('Sair') }}
                   </a>

                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        @endguest

    </ul>
</div>
</nav>

<main class="py-4">
    @yield('content')
</main>
</div>
</body>
</html>
