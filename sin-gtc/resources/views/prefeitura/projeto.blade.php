<?php

use App\Entidade;
use App\Projeto;

?>
@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-light"> <h2> Projetos</h2></div>

                <div class="card-body">

                    <div id="accordionFilter">

                        <div class="mb-0">
                            <button class="btn btn-link bg-light" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none;">
                                <b class="text-dark"> Filtros </b><i class="fas fa-filter btn-sm text-dark"></i>
                            </button>

                            <hr/>

                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFilter">

                            <div class="jumbotron jumbotron-fluid bg-light">

                                <div class="container">

                                    <form action="" method="post">
                                        {{ csrf_field() }}
                                        <div class="row col-12" style="margin-top: -35px;">
                                            <div class="col-lg-3">
                                                <strong>
                                                    <label for="nome">Projeto</label>
                                                </strong>
                                                <input type="text" name="projeto" id="projeto" class="form-control" autofocus><br>
                                            </div>
                                            <div class="col-lg-3">
                                                <strong>
                                                    <label for="entidade">Entidade</label>
                                                </strong>
                                                <input type="text" name="entidade" id="entidade" class="form-control"><br>
                                            </div>

                                            <div class="col-lg-2">
                                                <strong>
                                                    <label for="situacao">Situação</label>
                                                </strong>
                                                <select name="situacao" id="situacao" class="form-control">
                                                    <option disabled selected>Selecione</option>
                                                    <option value="1">Aprovado</option>
                                                    <option value="0">Pendente</option>
                                                </select><br>
                                            </div>

                                            <div class="col-lg-2">
                                                <strong>
                                                    <label for="inicio">Data Inicial</label>
                                                </strong>
                                                <input type="date" name="inicio" id="inicio" class="form-control"><br>
                                            </div>

                                            <div class="col-lg-2">
                                                <strong>
                                                    <label for="final">Data Final</label>
                                                </strong>
                                                <input type="date" name="final" id="final" class="form-control"><br>
                                            </div>

                                        </div>
                                        <hr/>
                                        <div class="container">

                                            <button type="submit" name="pesquisar" title="Pesquisar" class="btn btn-success" value="Pesquisar">
                                                <i class="fas fa-search"></i>
                                                Pesquisar

                                            </button>

                                            <button type="reset" name="limpar" title="Limpar pesquisa" class="btn btn-success" value="Limpar">
                                                <i class="fas fa-undo-alt"></i>
                                                Limpar

                                            </button>
                                        </div>

                                    </form>


                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
                                <tr>
                                    <th> Projeto </th>
                                    <th> Entidade </th>
                                    <th> Situação </th>
                                    <th> Data Inicial </th>
                                    <th> Data Final </th>
                                    <th colspan="3" width="1%"> Ações </th>


                                </tr>
                            </thead>


                            <tbody style="text-align: center;">
                                <td> exemplo </td>
                                <td> exemplo </td>
                                <td> exemplo </td>
                                <td> exemplo </td>
                                <td> exemplo </td>


                                <td>
                                    <a href="{{route('parecer')}}" class="btn btn-muted btn-lg text-dark" title="Parecer">
                                        <i class="fas fa-file-alt"></i>

                                    </a>
                                </td>

                                <td>
                                    <a href="#" class="btn btn-muted btn-lg" title="Download Documento">
                                        <i class="fas fa-arrow-alt-circle-down"></i>
                                    </a>
                                </td>
                                @php($projetos = Projeto::whereNotNull('idRequerente')->get())
                                @foreach($projetos as $projeto)
                            </tbody>
                            <tbody style="text-align: center;">
                                <td>
                                    <?php
                                    switch ($projeto->tipoRequerimento){
                                        case 1:
                                        echo 'Análise de Projeto e Registro de Estabelecimento';
                                        break;
                                        case 2:
                                        echo 'Instalação do SIM - Serviço de Inspeção Municipal no Estabelecimento';
                                        break;
                                        case 3:
                                        echo 'Licença para solicitação de ampliação de Estabelecimento';
                                        break;
                                        case 4:
                                        echo 'Análise de Projeto e Registro de Estabelecimento';
                                        break;
                                    }
                                    ?>
                                </td>
                                <td>
                                    @php($entidade = Entidade::findOrFail($projeto->idEntidade))
                                    {{$entidade->nomeEntidade}}
                                </td>
                                <td>
                                    <?php
                                    switch ($projeto->situacao){
                                        case 1:
                                        echo 'Pendente';
                                        break;
                                        case 1:
                                        echo 'Aprovado';
                                        break;

                                    }
                                    ?>
                                </td>
                                <td> {{date( 'd-m-Y', strtotime($projeto->created_at) )}} </td>
                                <td>
                                    <?php
                                    if($projeto->dataFinal != null){
                                        echo date( 'd-m-Y', strtotime($projeto->dataFinal) );
                                    }
                                    else{
                                        echo 'Não Finalizado';
                                    }
                                    ?>
                                </td>


                                <td>
                                    <a href="{{route('parecer')}}" class="btn btn-muted btn-lg text-dark" title="Parecer">
                                        <i class="fas fa-file-alt"></i>

                                    </a>
                                </td>

                                <td>
                                    <a href="#" class="btn btn-muted btn-lg" title="Download Documento">
                                        <i class="fas fa-arrow-alt-circle-down"></i>
                                    </a>
                                </td>
                            </tbody>
                            @endforeach

                        </table>
                    </div>
                    <br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
