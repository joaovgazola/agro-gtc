@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-12">         
      <div class="card">
        <div class="card-header bg-light"> <h2> Parecer </h2></div>

        <div class="card-body">
         <div class="row">
           
           <div class="col-lg-4">
            <strong>
              <label for="entidade">Nome da Entidade</label>
            </strong>

            <input type="text" name="entidade" id="entidade" class="form-control" disabled readonly value="NOSSO LAR ABRIGO PARA IDOSO"><br>
          </div>

          <div class="col-lg-4">
            <strong>
              <label for="projeto">Nome do Projeto</label>
            </strong>

            <input type="text" name="projeto" id="projeto" class="form-control" disabled readonly value="Custeio Atendimento Idoso NOSSO LAR"><br>
          </div>

          <div class="col-lg-2">
            <strong>
              <label for="situacao">Situação do Projeto</label>
            </strong>

            <input type="text" name="situacao" id="situacao" class="form-control" readonly value="Aprovado"><br>
          </div>

          <div class="col-lg-2">
            <strong>
              <label for="inicio">Data do Projeto</label>
            </strong>

            <input type="date" name="inicio" id="inicio" class="form-control" readonly><br>
          </div>
        </div>

        <hr/>
        <form>
          <div class="row">
           
           <div class="col-lg-10">

            <strong>
              <label for="parecer">Parecer</label>
            </strong>
            <textarea class="form-control" rows="2" cols="50" name="parecer" id="parecer" required>
            </textarea>
          </div>

          <div class="col-lg-2">

            <strong>
              <label for="decisao">Decisão</label>
            </strong>
            <div class="form-check">
             <input class="form-check-input" type="radio" name="aprovado" id="aprovado" value="aprovado" required>
             <label class="form-check-label" for="aprovado">
               Aprovado
             </label>
           </div>
           <div class="form-check">
             <input class="form-check-input" type="radio" name="pendente" id="pendente" value="pendente">
             <label class="form-check-label" for="pendente">
               Pendente
             </label>
           </div>

         </div>
         <br>
         
       </div>
       <br>
       <hr/>
       <div class="row float-right mr-0">

        <input type="submit" name="salvar" class="btn btn-primary" value="Confimar">

      </div>
    </form>
  </div>
</div>
</div>
</div>
</div>
@endsection
