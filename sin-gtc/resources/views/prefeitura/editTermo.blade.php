@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4>Editar Termos de Requerimento</h4></div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif



                    <div id="editor">
                    </div>
                    <br>
                    <button class="btn btn-success float-left" onclick="window.location.href = '/admin'">Voltar</button>
                    <form action="{{route('saveTermo')}}" id="saveTermo">
                        @csrf
                        <textarea name="hiddenArea" style="display:none" id="hiddenArea"></textarea>
                        <input type="submit" value="Salvar" class="btn btn-success float-right"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<script>

    var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote', 'code-block'],

    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'indent': '-1'}, { 'indent': '+1' }],
    [{ 'direction': 'rtl' }],
    [{ 'size': ['small', false, 'large', 'huge'] }],
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'color': [] }, { 'background': [] }],
    [{ 'font': [] }],
    [{ 'align': [] }],
    ['clean']
    ];
    var quill = new Quill('#editor', {
        theme: 'snow',
        modules: {
            toolbar : toolbarOptions
        }

    });
    $("#saveTermo").on("submit",function(){
        $("#hiddenArea").val(JSON.stringify(quill.getContents()));
    })
    <?php
    if(!empty($termo->delta)){
        echo 'quill.setContents('.$termo->delta.')';
    }
    ?>
</script>

@endsection
