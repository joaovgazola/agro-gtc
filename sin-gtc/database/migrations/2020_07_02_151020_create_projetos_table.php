<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idRequerente')->nullable();
            $table->tinyInteger('tipoRequerimento')->nullable();
            $table->unsignedBigInteger('idEntidade')->nullable();
            $table->tinyInteger('situacao')->default('1');
            $table->string('pastaDocumentos')->nullable();
            $table->foreign('idRequerente')->references('id')->on('representantes');
            $table->foreign('idEntidade')->references('id')->on('entidades');
            $table->date('dataFinal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projetos');
    }
}
