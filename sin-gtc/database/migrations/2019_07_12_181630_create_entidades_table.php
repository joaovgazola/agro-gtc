<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomeEntidade');
            $table->string('cnpj')->unique();
            $table->string('atividade');
            $table->string('descAtividade')->nullable();
            $table->string('valor');
            $table->string('tipoLicenca')->nullable();
            $table->string('numeroLicenca')->nullable();
            $table->date('anoLicenca')->nullable();
            $table->date('validadeLicenca')->nullable();
            $table->unsignedBigInteger('idLocal');
            $table->unsignedBigInteger('idRepresentante')->nullable();
            $table->foreign('idLocal')->references('id')->on('locals')->onDelete('cascade');
            $table->foreign('idRepresentante')->references('id')->on('representantes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entidades');
    }
}
