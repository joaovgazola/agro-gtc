<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomeRepresentante');
            $table->string('cpfRepresentante')->unique();
            $table->string('cargoRepresentante');
            $table->string('telefoneRepresentante');
            $table->string('faxRepresentante');
            $table->string('celularRepresentante');
            $table->string('emailRepresentante');
            $table->unsignedBigInteger('idLocal');
            $table->foreign('idLocal')->references('id')->on('locals')->onDelete('cascade');
            $table->tinyInteger('autorizado')->default('1');
            $table->string('nomenao')->nullable();
            $table->string('cpfnao')->nullable();
            $table->string('telefonenao')->nullable();
            $table->string('emailnao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
