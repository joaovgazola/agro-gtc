<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/editTermo', 'AdminController@editTermo')->name('editTermo');
Route::any('/admin/projeto', 'AdminController@projeto')->name('projeto');
Route::get('/admin/editTermo/save', 'AdminController@saveTermo')->name('saveTermo');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('adminLogin');
Route::post('/admin/login/submit', 'Auth\AdminLoginController@login')->name('adminLoginSubmit');

Route::any('/admin/parecer', 'AdminController@parecer')->name('parecer');
Route::any('/entidade', 'Auth\EmpresaRegisterController@entidade')->name('entidade');
Route::any('/licenca', 'Auth\EmpresaRegisterController@licenca')->name('licenca');
Route::any('/representante', 'Auth\EmpresaRegisterController@representante')->name('representante');
Route::any('/requerente', 'Auth\EmpresaRegisterController@requerente')->name('requerente');
Route::any('/requerimento', 'Auth\EmpresaRegisterController@requerimento')->name('requerimento');
Route::any('/tipoprojeto', 'Auth\EmpresaRegisterController@tipoprojeto')->name('tipoprojeto');
Route::any('/usuario', 'Auth\EmpresaRegisterController@usuario')->name('usuario');
Route::any('/usuario/salvar', 'Auth\EmpresaRegisterController@register')->name('registerEmpresa');


